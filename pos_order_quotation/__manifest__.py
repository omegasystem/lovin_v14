{
    'name': 'Pos Quotations',
    'version': '14.0.1.0.3',
    'category': 'Sales/Point of Sale',
    'sequence': 6,
    'license': 'LGPL-3',
    'summary': 'Create Quotation, Save Quotation and Load Quotation Order in Point Of Sale (Pos). Pos order Pos Note for quotation order',
    'description': """
        
        """,
    'author': "AB Tech",
    'website': 'abtechsolution.in',
    'depends': [
        'mail',
        'resource',
        'fetchmail',
        'utm',
        'contacts',
        'digest',
        'phone_validation',
        'point_of_sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/assests.xml',
        'views/pos_quotation.xml',
        'data/ir_sequence.xml',
        'views/pos_order.xml',
        'views/workshop.xml',
        'views/qc.xml'
    ],
    'qweb': [
        'static/src/xml/LoadQuotationButton.xml',
        'static/src/xml/CreateQuotationButton.xml',
        'static/src/xml/SaveQuotationPopUp.xml',
        'static/src/xml/LoadQuotationPopup.xml',
        'static/src/xml/OrderReceipt.xml',
        'static/src/xml/AlertPopups.xml',
    ],
    "images": ["static/description/banner.gif"],
    'installable': True,
}
