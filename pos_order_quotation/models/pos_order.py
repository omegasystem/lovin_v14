from odoo import fields, models, api, _
import base64
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import ValidationError


class PosOrder(models.Model):
    _inherit = 'pos.order'

    quotation_id = fields.Many2one('pos.quotation')
    quotation_name = fields.Char("Quotation Name")
    seller_id = fields.Many2one("res.users", string="Seller")

    @api.model
    def _order_fields(self, ui_order):
        data = super(PosOrder, self)._order_fields(ui_order)
        data.update({
            'quotation_id': ui_order.get('quotation_id') or False,
            'quotation_name': ui_order.get('quotation_name') or False,
            'seller_id': ui_order.get('seller_id') or False,
        })
        return data

    @api.model
    def create(self, vals):
        orders = super(PosOrder, self).create(vals)
        if orders.quotation_id:
            orders.quotation_id.write({'state': 'loaded'})
        return orders

    def action_receipt_to_customer(self, name, client, ticket):
        if not self:
            return False
        if not client.get('email'):
            return False

        message = _("<p>Dear %s,<br/>Here is your electronic ticket for the %s. </p>") % (client['name'], name)
        filename = 'Receipt-' + name + '.jpg'
        vals = {
            'name': filename,
            'type': 'binary',
            'datas': ticket,
            'res_model': 'pos.order',
            'store_fname': filename,
            'mimetype': 'image/jpeg',
        }
        if self.ids:
            vals['res_id'] = self.ids[0]
        receipt = self.env['ir.attachment'].create(vals)
        mail_values = {
            'subject': _('Receipt %s', name),
            'body_html': message,
            'author_id': self.env.user.partner_id.id,
            'email_from': self.env.company.email or self.env.user.email_formatted,
            'email_to': client['email'],
            'attachment_ids': [(4, receipt.id)],
        }

        if self.mapped('account_move') and self.ids:
            report = self.env.ref('point_of_sale.pos_invoice_report')._render_qweb_pdf(self.ids[0])
            filename = name + '.pdf'
            attachment = self.env['ir.attachment'].create({
                'name': filename,
                'type': 'binary',
                'datas': base64.b64encode(report[0]),
                'store_fname': filename,
                'res_model': 'pos.order',
                'res_id': self.ids[0],
                'mimetype': 'application/x-pdf'
            })
            mail_values['attachment_ids'] += [(4, attachment.id)]

        mail = self.env['mail.mail'].sudo().create(mail_values)
        mail.send()


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.constrains('vat', 'country_id')
    def check_vat(self):
        if self.env.context.get('company_id'):
            company = self.env['res.company'].browse(self.env.context['company_id'])
        else:
            company = self.env.company
        eu_countries = self.env.ref('base.europe').country_ids
        for partner in self:
            if not partner.vat:
                continue

            if company.vat_check_vies and partner.commercial_partner_id.country_id in eu_countries:
                # force full VIES online check
                check_func = self.vies_vat_check
            else:
                # quick and partial off-line checksum validation
                check_func = self.simple_vat_check

            failed_check = False
            # check with country code as prefix of the TIN
            vat_country_code, vat_number = self._split_vat(partner.vat)
            vat_guessed_country = self.env['res.country'].search([('code', '=', vat_country_code.upper())])
            if vat_guessed_country:
                failed_check = not check_func(vat_country_code, vat_number)

            # if fails, check with country code from country
            partner_country_code = partner.commercial_partner_id.country_id.code
            if (not vat_guessed_country or failed_check) and partner_country_code:
                failed_check = not check_func(partner_country_code.lower(), partner.vat)

            # We allow any number if it doesn't start with a country code and the partner has no country.
            # This is necessary to support an ORM limitation: setting vat and country_id together on a company
            # triggers two distinct write on res.partner, one for each field, both triggering this constraint.
            # If vat is set before country_id, the constraint must not break.

            if failed_check:
                country_code = partner_country_code or vat_country_code
                if country_code != 'GB':
                    print("cal----------", country_code)
                    msg = partner._construct_constraint_msg(country_code.lower() if country_code else None)
                    raise ValidationError(msg)

    # def _construct_constraint_msg(self, country_code):
    #     print("country_code--------------", country_code)
    #     self.ensure_one()
    #     if country_code != 'gb':
    #         vat_no = "'CC##' (CC=Country Code, ##=VAT Number)"
    #         vat_no = _ref_vat.get(country_code) or vat_no
    #         if self.env.context.get('company_id'):
    #             company = self.env['res.company'].browse(self.env.context['company_id'])
    #         else:
    #             company = self.env.company
    #         if company.vat_check_vies:
    #             return '\n' + _(
    #                 'The VAT number [%(vat)s] for partner [%(name)s] either failed the VIES VAT validation check or did not respect the expected format %(format)s.',
    #                 vat=self.vat,
    #                 name=self.name,
    #                 format=vat_no
    #             )
    #         return '\n' + _(
    #             'The VAT number [%(vat)s] for partner [%(name)s] does not seem to be valid. \nNote: the expected format is %(format)s',
    #             vat=self.vat,
    #             name=self.name,
    #             format=vat_no
    #         )