# -*- coding: utf-8 -*-
from odoo import models, fields, api


class LovinBagsQc(models.Model):
    _name = 'lovin.bags.qc'
    _rec_name = 'pos_ref'
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'utm.mixin',
    ]

    color = fields.Integer('Color Index', default=0)
    workshop_id = fields.Many2one('lovin.bags.workshop', string='Workshop')
    pos_quote_id = fields.Many2one('pos.quotation', string='Quote ID')
    pos_ref = fields.Char(related='pos_quote_id.ref', string='POS Ref')
    date = fields.Datetime(string='Quote Date')
    assign_to = fields.Many2one('res.users', string='Assign To')
    qc_state = fields.Selection([
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('cancelled', 'Cancelled'),
        ('done', 'Done')
    ])
    product_id = fields.Many2one('product.product', string='Product')
    attachment_ids = fields.Many2many('ir.attachment', string='Attachments')
    current_status = fields.Selection([
        ('in_store', 'In Store'),
        ('send_workshop', 'Send Workshop'),
        ('in_workshop', 'In Workshop'),
        ('send_qc', 'Send QC'),
        ('in_qc', 'In QC'),
        ('dispatch_workshop', 'Dispatch Workshop'),
        ('received_store', 'Received Store'),
    ])
    note = fields.Html('Note')
    start_date_time = fields.Datetime('Job Start Time')
    end_date_time = fields.Datetime('Job End Time')
    quote_line_ref = fields.Many2one('pos.quotation.line', string='Quote Line Ref')

    def send_to_workshop(self):
        self.current_status = 'send_workshop'

    def received_workshop(self):
        self.current_status = 'in_workshop'

    def send_to_qc(self):
        self.current_status = 'send_qc'

    def received_qc(self):
        self.current_status = 'in_qc'

    def dispatch_qc(self):
        print()

    def dispatch_workshop(self):
        self.current_status = 'dispatch_workshop'

    def received_store(self):
        self.current_status = 'received_store'

    def set_to_approve(self):
        self.qc_state = 'approved'

    def set_to_reject(self):
        self.qc_state = 'rejected'
        self.quote_line_ref.qc_state = 'rejected'
        self.workshop_id.qc_status = 'rejected'

    def set_to_cancel(self):
        self.qc_state = 'cancelled'

    def start_job(self):
        self.start_date_time = fields.Datetime.now()

    def end_job(self):
        self.end_date_time = fields.Datetime.now()
