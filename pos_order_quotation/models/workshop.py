# -*- coding: utf-8 -*-
from odoo import models, fields, api

AVAILABLE_PRIORITIES = [
    ('0', 'Low'),
    ('1', 'Medium'),
    ('2', 'High'),
    ('3', 'Very High'),
]


class LovinBagsWorkShop(models.Model):
    _name = 'lovin.bags.workshop'
    _rec_name = 'pos_ref'
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'utm.mixin',
    ]

    color = fields.Integer('Color Index', default=0)
    pos_quote_id = fields.Many2one('pos.quotation', string='Quote ID')
    pos_ref = fields.Char(related='pos_quote_id.ref', string='POS Ref')
    date = fields.Datetime(string='Quote Date')
    assign_to = fields.Many2one('res.users', string='Assign To')
    workshop_state = fields.Selection([
        ('draft', 'Draft'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('cancelled', 'Cancelled'),
        ('done', 'Done')
    ], default='draft', string='Workshop State')
    qc_status = fields.Selection([
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('cancelled', 'Cancelled'),
        ('done', 'Done')
    ], string='QC State')
    priority = fields.Selection(
        AVAILABLE_PRIORITIES, string='Priority', index=True)
    tag_ids = fields.Many2many(
        'crm.tag', string='Tags', help="Classify and analyze your lead/opportunity categories like: Training, Service")
    product_id = fields.Many2one('product.product', string='Product')
    attachment_ids = fields.Many2many('ir.attachment', string='Attachments')
    current_status = fields.Selection([
        ('in_store', 'In Store'),
        ('send_workshop', 'Send Workshop'),
        ('in_workshop', 'In Workshop'),
        ('send_qc', 'Send QC'),
        ('in_qc', 'In QC'),
        ('dispatch_workshop', 'Dispatch Workshop'),
        ('received_store', 'Received Store'),
    ])
    note = fields.Html('Note')
    start_date_time = fields.Datetime('Job Start Time')
    end_date_time = fields.Datetime('Job End Time')
    points = fields.Integer('Job Points')
    quote_line_ref = fields.Many2one('pos.quotation.line', string='Quote Line Ref')
    is_job_start = fields.Boolean('Is Job Start')
    is_job_end = fields.Boolean('Is Job End')



    def send_to_workshop(self):
        self.current_status = 'send_workshop'

    def received_workshop(self):
        self.current_status = 'in_workshop'

    def send_to_qc(self):
        self.current_status = 'send_qc'
        self.env['lovin.bags.qc'].create({
            'pos_quote_id': self.pos_quote_id.id,
            'workshop_id': self.id,
            'product_id': self.product_id.id,
            'date': self.date,
            'quote_line_ref': self.quote_line_ref.id,
            'attachment_ids': [(4, attachment_id.id) for attachment_id in self.attachment_ids]
        })

    def received_qc(self):
        self.current_status = 'in_qc'

    def dispatch_qc(self):
        print()

    def dispatch_workshop(self):
        self.current_status = 'dispatch_workshop'

    def received_store(self):
        self.current_status = 'received_store'

    def set_to_approve(self):
        self.workshop_state = 'approved'

    def set_to_reject(self):
        self.workshop_state = 'rejected'

    def set_to_cancel(self):
        self.workshop_state = 'cancelled'

    def start_job(self):
        self.is_job_start = True
        self.start_date_time = fields.Datetime.now()

    def end_job(self):
        self.is_job_end = True
        self.end_date_time = fields.Datetime.now()
