odoo.define('pos_order_quotation.SaveQuotationPopUp', function(require) {
    'use strict';

    const {
        useState,
        useRef
    } = owl.hooks;
    const AbstractAwaitablePopup = require('point_of_sale.AbstractAwaitablePopup');
    const Registries = require('point_of_sale.Registries');

    class SaveQuotationPopUp extends AbstractAwaitablePopup {
        constructor() {
            super(...arguments);
            this.state = useState({
                inputValue: this.props.startingValue,
                quotationNumber: this.props.quotationNumber,
                customer: this.props.customer,
                assessmentsid: this.props.assessmentsid,
                assignees: this.props.assignees

            });
            this.inputRef = useRef('textarea');
        }
        mounted() {
            this.inputRef.el.focus();
        }
        getPayload() {
            console.log("this.state---------", this.state);
            var selected_vals = [];
            var assessment = this.state.assessment
            var inputValue = this.state.inputValue
            var assessment_type = this.state.assessment_type
            selected_vals.push(assessment);
            selected_vals.push(inputValue);
            selected_vals.push(assessment_type);
            return selected_vals
//            return this.state.inputValue
        }

        async confirmPrint() {
            this.props.resolve({
                confirmed: true,
                payload: await this.getPayload(),
                print: true
            });
            this.trigger('close-popup');
        }

        _cancelAtEscape(event) {
            super._cancelAtEscape(event);
            if (event.key === 'Enter') {
                this.confirm();
            }

        }
    }

    SaveQuotationPopUp.template = 'SaveQuotationPopUp';
    SaveQuotationPopUp.defaultProps = {
        confirmText: 'Save',
        cancelText: 'Cancel',
        title: '',
        body: '',
        startingValue: '',
    };

    Registries.Component.add(SaveQuotationPopUp);

    return SaveQuotationPopUp;
});